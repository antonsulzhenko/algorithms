package com.antonsulzhenko.algorithms.collections.trees.redblacktree;

/**
 * The class RedBlackTree.
 *
 * @param <V> the value type parameter
 * @author Anton Sulzhenko
 */
public class RBTree<V extends Comparable<V>> {
    private static boolean RED = false;
    private static boolean BLACK = true;

    private RBNode root;

    /**
     * Add red black node.
     *
     * @param value the value
     * @return the created red black node
     */
    public RBNode add(V value) {
        if (value == null) {
            throw new NullPointerException();
        }

        if (root == null) {
            this.root = new RBNode(value, null);
            return root;
        }

        RBNode parent = root;
        RBNode newNode = null;
        while (newNode == null) {
            if (parent.left == null && parent.right == null) {
                break;
            }

            int cmp = compare(value, parent.value);
            if (cmp < 0) {
                parent = parent.left;
            } else if (cmp > 0) {
                parent = parent.right;
            } else {

            }
        }

//        RBNode newNode = new RBNode(value, parent);
//
//        int cmp = compare(value, parent.value);
//        if (cmp < 0) {
//            parent.left = newNode;
//        } else if (cmp > 0) {
//            parent.right = newNode;
//        } else {
//            return parent; // this value already exists
//        }

        rebuildAfterInsert();

        return newNode;
    }

    /**
     * Gets root.
     *
     * @return the root
     */
    public RBNode getRoot() {
        return root;
    }

    private void rebuildAfterInsert() {
        //TODO: implement tree rebuilding after insertion
    }

    private int compare(V v1, V v2) {
        return v1.compareTo(v2);
    }

    /**
     * The Red black node.
     */
    public class RBNode {
        private RBNode parent;
        private RBNode left;
        private RBNode right;

        private V value;

        private boolean color;

        /**
         * Instantiates a new Red black node.
         *
         * @param value the value
         * @param parent the parent node
         */
        private RBNode(V value, RBNode parent) {
            this.parent = parent;
            this.value = value;
            // root node is 'BLACK' by default
            if (this.parent == null) {
                this.color = BLACK;
            }
        }

        /**
         * Gets parent.
         *
         * @return the parent
         */
        public RBNode getParent() {
            return parent;
        }

        /**
         * Gets left.
         *
         * @return the left
         */
        public RBNode getLeft() {
            return left;
        }

        /**
         * Gets right.
         *
         * @return the right
         */
        public RBNode getRight() {
            return right;
        }

        /**
         * Gets value.
         *
         * @return the value
         */
        public V getValue() {
            return value;
        }

        /**
         * Is black.
         *
         * @return the boolean
         */
        public boolean isBlack() {
            return color;
        }
    }
}