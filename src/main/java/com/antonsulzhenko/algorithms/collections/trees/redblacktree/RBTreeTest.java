package com.antonsulzhenko.algorithms.collections.trees.redblacktree;

import static com.antonsulzhenko.algorithms.collections.trees.redblacktree.RBTreePrinter.printNode;

/**
 * The class RedBlackTreeTest.
 *
 * @author Anton Sulzhenko
 */
public class RBTreeTest {
    public static void main(String[] args) {
        RBTree<Integer> rbt = new RBTree<>();
        rbt.add(10);
        rbt.add(7);
        rbt.add(12);

        printNode(rbt.getRoot());

    }
}
