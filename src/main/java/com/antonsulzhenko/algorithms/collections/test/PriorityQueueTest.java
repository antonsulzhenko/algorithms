package com.antonsulzhenko.algorithms.collections.test;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * The class PriorityQueueTest.
 *
 * @author Anton Sulzhenko
 */
public class PriorityQueueTest {
    public static void main(String[] args) {
//        prioritySample();
        comparatorSample();
    }

    private static void comparatorSample() {
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>(10, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 % 2 == 0 && o2 % 2 != 0) {
                    return -1;
                }
                if (o1 % 2 != 0 && o2 % 2 == 0) {
                    return 1;
                }

                if (o1 < o2) {
                    return -1;
                }
                if (o1 > o2) {
                    return 1;
                }

                return 0;
            }
        });

        pq.add(5);
        pq.add(2);
        pq.add(1);
        pq.add(4);

        while (!pq.isEmpty()) {
            System.out.println(pq.poll());
        }
    }

    private static void prioritySample() {
        Queue<Integer> q = new LinkedList<Integer>();
        q.add(4);
        q.add(1);
        q.add(3);
        q.add(5);
        q.add(2);

        while (!q.isEmpty()) {
            System.out.println(q.poll());
        }

        System.out.println("==================");

        PriorityQueue<Integer> pq = new PriorityQueue<Integer>();
        pq.add(4);
        pq.add(1);
        pq.add(3);
        pq.add(5);
        pq.add(2);

        while (!pq.isEmpty()) {
            System.out.println(pq.poll());
        }
    }
}
