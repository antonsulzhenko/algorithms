package com.antonsulzhenko.algorithms.collections.test;

import java.util.HashMap;

/**
 * The class HashMapTest.
 *
 * @author Anton Sulzhenko
 */
public class HashMapTest {
    public static void main(String[] args) {
        HashMap<Entity, String> map = new HashMap<Entity, String>();
        map.put(new Entity("1"), "1");
        map.put(new Entity("2"), "2");
        map.put(new Entity("3"), "3");
        map.put(new Entity("4"), "4");
        map.put(new Entity("5"), "5");
        map.put(new Entity("6"), "6");
        map.put(new Entity("7"), "7");
        map.put(new Entity("8"), "8");
        map.put(new Entity("9"), "9");
        map.put(new Entity("10"), "10");
        map.put(new Entity("11"), "11");
        map.put(new Entity("12"), "12");
        map.put(new Entity("13"), "13");
        map.put(new Entity("14"), "14");
        map.put(new Entity("15"), "15");
        map.put(new Entity("16"), "16");
        map.put(new Entity("17"), "17");
        System.out.printf(String.valueOf(map));
    }
}

class Entity {

    private String value;

    Entity(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Entity)) return false;

        Entity entity = (Entity) o;

        if (value != null ? !value.equals(entity.value) : entity.value != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return 7;
    }
}
