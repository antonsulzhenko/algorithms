package com.antonsulzhenko.algorithms.collections.test;

import java.util.Date;
import java.util.WeakHashMap;

/**
 * The class WeakHashMapTest.
 *
 * @author Anton Sulzhenko
 */
public class WeakHashMapTest {
    public static void main(String[] args) {
        WeakHashMap<Date, String> weakMap = new WeakHashMap<Date, String>();
        Date date = new Date();
        weakMap.put(date, "info");

        date = null;
//        System.gc();
        System.gc();

        int counter = 0;
        for (;;) {
            counter++;
            if (weakMap.isEmpty()) {
                System.out.println("Map is empty! Counter: " + counter);
                break;
            } /*else {
                System.out.println("not empty");
            }*/
        }
    }
}
