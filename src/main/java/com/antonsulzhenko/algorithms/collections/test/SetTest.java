package com.antonsulzhenko.algorithms.collections.test;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The class SetTest.
 *
 * @author Anton Sulzhenko
 */
public class SetTest {
    public static void main(String[] args) {
        Set<Integer> set = new TreeSet<Integer>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return 0;
            }
        });

        set.add(1);
        set.add(2);
        set.add(1);

        System.out.println(set);

        sortedSetTest();

        test();
    }

    private static void test() {
        SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        System.out.println(DATE_FORMATTER.format(new Date()));
    }

    private static void sortedSetTest() {
        NavigableSet<Integer> ns = new TreeSet<Integer>();
        for (int i = 0; i < 10; i++) {
            ns.add(i);
        }

        System.out.println(ns.lower(3));
        System.out.println(ns.higher(5));
    }
}
