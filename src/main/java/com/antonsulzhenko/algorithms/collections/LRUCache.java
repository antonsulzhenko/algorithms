package com.antonsulzhenko.algorithms.collections;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The class LRUCache.
 *
 * @author Anton Sulzhenko
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    private final int initialCapacity;

    public LRUCache(int initialCapacity) {
        super(initialCapacity + 1, 1.1f, true);
        this.initialCapacity = initialCapacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > initialCapacity;
    }

    public static void main(String[] args) {
        LRUCache<Integer, Integer> cache = new LRUCache<Integer, Integer>(2);
        cache.put(1,1);
        cache.put(2,2);
        cache.put(3,3);
        cache.get(2);
        cache.put(4,4);
        System.out.println(cache);
    }
}
