package com.antonsulzhenko.algorithms.memory;

import java.util.ArrayList;
import java.util.UUID;

/**
 * The class MemoryLeak.
 *
 * @author Anton Sulzhenko
 */
public class MemoryLeak {

    private static final ArrayList<String> str = new ArrayList<String>();

    public static void main(String[] args) {
        for (;;) {
            str.add(UUID.randomUUID().toString());
        }
    }
}
