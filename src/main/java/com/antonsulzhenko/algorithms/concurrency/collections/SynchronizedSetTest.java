package com.antonsulzhenko.algorithms.concurrency.collections;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;

/**
 * The class SynchronizedSetTest.
 *
 * @author Anton Sulzhenko
 */
public class SynchronizedSetTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Set<Integer> set1 = Collections.synchronizedSet(new HashSet<Integer>());
        Set<Integer> set2 = new CopyOnWriteArraySet<Integer>();
        Set<Integer> set3 = new ConcurrentSkipListSet<Integer>();

        System.out.println("synchronizedSet:");
        testSet(set1);

        System.out.println("CopyOnWriteArraySet:");
        testSet(set2);

        System.out.println("ConcurrentSkipListSet:");
        testSet(set3);
    }

    private static void testSet(Set<Integer> set) throws InterruptedException, ExecutionException {
        CountDownLatch latch = new CountDownLatch(1);

        ExecutorService service = Executors.newFixedThreadPool(4);

        Future<OperationInfo> t1 = service.submit(new SetRunner(latch, set, 0, 25000));
        Future<OperationInfo> t2 = service.submit(new SetRunner(latch, set, 25000, 50000));
        Future<OperationInfo> t3 = service.submit(new SetRunner(latch, set, 50000, 75000));
        Future<OperationInfo> t4 = service.submit(new SetRunner(latch, set, 75000, 100000));

        latch.countDown();

        System.out.println("T1 - removeTime: " + t1.get().removeTime /1000
                + ", writeTime: " + t1.get().writeTime/1000
                + ", readTime: " + t1.get().readTime/1000);
        System.out.println("T2 - removeTime: " + t2.get().removeTime /1000
                + ", writeTime: " + t2.get().writeTime/1000
                + ", readTime: " + t2.get().readTime/1000);
        System.out.println("T3 - removeTime: " + t3.get().removeTime /1000
                + ", writeTime: " + t3.get().writeTime/1000
                + ", readTime: " + t3.get().readTime/1000);
        System.out.println("T4 - removeTime: " + t4.get().removeTime /1000
                + ", writeTime: " + t4.get().writeTime/1000
                + ", readTime: " + t4.get().readTime/1000);

        service.shutdown();
    }

    static class SetRunner implements Callable<OperationInfo> {
        CountDownLatch latch;
        Set<Integer> set;
        int from;
        int to;

        SetRunner(CountDownLatch latch, Set<Integer> set, int from, int to) {
            this.latch = latch;
            this.set = set;
            this.from = from;
            this.to = to;
        }

        @Override
        public OperationInfo call() throws Exception {
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            OperationInfo oi = new OperationInfo();

            long start = System.nanoTime();

            for (int i = from ; i < to; i++) {
                set.add(i);
            }

            oi.writeTime = System.nanoTime() - start;

            start = System.nanoTime();

            for (int i = from ; i < to; i++) {
                set.contains(i);
            }

            oi.readTime = System.nanoTime() - start;

            start = System.nanoTime();

            for (int i = from ; i < to; i++) {
                set.remove(i);
            }

            oi.removeTime = System.nanoTime() - start;

            return oi;
        }
    }

    static class OperationInfo {
        long removeTime;
        long writeTime;
        long readTime;
    }
}
