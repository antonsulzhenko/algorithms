package com.antonsulzhenko.algorithms.concurrency.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * The class SynchronizedListTest.
 *
 * @author Anton Sulzhenko
 */
public class SynchronizedListTest {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        List<String> list1 = Collections.synchronizedList(new ArrayList<String>());
        List<String> list2 = new CopyOnWriteArrayList<String>();

        System.out.println("Sync list:");
        testList(list1);

        System.out.println("CoW list:");
        testList(list2);
    }

    private static void testList(List<String> list) throws InterruptedException, ExecutionException {
        fillList(list, 100000);

        CountDownLatch latch = new CountDownLatch(1);

        ExecutorService service = Executors.newFixedThreadPool(4);

        Future<Long> t1 = service.submit(new ListRunner(latch, list, 0, 25000));
        Future<Long> t2 = service.submit(new ListRunner(latch, list, 25000, 50000));
        Future<Long> t3 = service.submit(new ListRunner(latch, list, 50000, 75000));
        Future<Long> t4 = service.submit(new ListRunner(latch, list, 75000, 100000));

        latch.countDown();

        System.out.println("T1: " + t1.get()/1000);
        System.out.println("T2: " + t2.get()/1000);
        System.out.println("T3: " + t3.get()/1000);
        System.out.println("T4: " + t4.get()/1000);

        service.shutdown();
    }

    private static void fillList(List<String> list, int count) {
        for (int i = 0; i < count; i++) {
            list.add(UUID.randomUUID().toString());
        }
    }

    static class ListRunner implements Callable<Long> {
        CountDownLatch latch;
        List list;
        int from;
        int to;

        ListRunner(CountDownLatch latch, List list, int from, int to) {
            this.latch = latch;
            this.list = list;
            this.from = from;
            this.to = to;
        }

        @Override
        public Long call() throws Exception {
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            long start = System.nanoTime();

            for ( ; from < to; from++) {
                list.get(from);
            }

            return System.nanoTime() - start;
        }
    }
}
