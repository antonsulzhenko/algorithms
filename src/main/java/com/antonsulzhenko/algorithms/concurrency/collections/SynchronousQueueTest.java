package com.antonsulzhenko.algorithms.concurrency.collections;

import com.antonsulzhenko.algorithms.concurrency.Utils;

import java.util.concurrent.*;

import static com.antonsulzhenko.algorithms.concurrency.collections.SynchronousQueueTest.randInt;
import static java.lang.Thread.sleep;

/**
 * The class SynchronousQueueTest.
 *
 * @author Anton Sulzhenko
 */
public class SynchronousQueueTest {

    private static final int MIN_RAND_INT = 0;
    private static final int MAX_RAND_INT = 30;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws ExecutionException the execution exception
     * @throws InterruptedException the interrupted exception
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        SynchronousQueue<Integer> sq = new SynchronousQueue<Integer>();

        ExecutorService service = Executors.newFixedThreadPool(2);

        int goal = randInt();

        Utils.println("Goal is number: " + goal);

        Future<?> t1 = service.submit(new Producer(sq, goal));
        Future<?> t2 = service.submit(new Consumer(sq, goal));

        t1.get();
        t2.get();

        service.shutdown();
    }

    /**
     * Rand int.
     *
     * @return the int
     */
    protected static int randInt() {
        return Utils.randInt(MIN_RAND_INT, MAX_RAND_INT);
    }
}

class Producer implements Runnable {

    SynchronousQueue<Integer> sq;
    int goal;

    Producer(SynchronousQueue<Integer> sq, int goal) {
        this.sq = sq;
        this.goal = goal;
    }

    @Override
    public void run() {
        while (true) {
            try {
                sleep(3000);
                int i = randInt();
                Utils.println("Trying to put: " + i);
                sq.put(i);
                if (i == goal) {
                    Utils.println("Finally! Exiting now :)");
                    return;
                }
            } catch (InterruptedException e) {
                Utils.println("I've got an error: " + e.getMessage());
                return;
            }
        }
    }
}

class Consumer implements Runnable {

    SynchronousQueue<Integer> sq;
    int goal;

    Consumer(SynchronousQueue<Integer> sq, int goal) {
        this.sq = sq;
        this.goal = goal;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Utils.println("Trying to take...");
                int number = sq.take();
                Utils.println("I've got: " + number + "!");
                if (goal == number) {
                    Utils.println("Finally! Exiting now :)");
                    return;
                }
            } catch (InterruptedException e) {
                Utils.println("I've got an error: " + e.getMessage());
                return;
            }
        }
    }
}
