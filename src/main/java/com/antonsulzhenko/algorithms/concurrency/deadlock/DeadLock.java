package com.antonsulzhenko.algorithms.concurrency.deadlock;

/**
 * The class DeadLock.
 *
 * @author Anton Sulzhenko
 */
public class DeadLock {
    static class Friend {
        private final String name;
        public Friend(String name) {
            this.name = name;
        }
        public String getName() {
            return this.name;
        }
        public synchronized void bow(Friend bower) {
            System.out.format("%s: %s has bowed to me!%n", this.name, bower.getName());
            bower.bowBack(this);
        }
        public synchronized void bowBack(Friend bower) {
            System.out.format("%s: %s has bowed back to me!%n", this.name, bower.getName());
        }
    }

    public static void main(String[] args) throws InterruptedException {
        final Friend alphonse = new Friend("Alphonse");
        final Friend gaston = new Friend("Gaston");

        Thread alphonseThread = new Thread(new Runnable() {
            public void run() {
                alphonse.bow(gaston);
            }
        });
        Thread gastonThread = new Thread(new Runnable() {
            public void run() {
                gaston.bow(alphonse);
            }
        });

        alphonseThread.start();
//        Thread.sleep(3000);
        gastonThread.start();

    }
}
