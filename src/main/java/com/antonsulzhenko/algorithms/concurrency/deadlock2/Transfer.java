package com.antonsulzhenko.algorithms.concurrency.deadlock2;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import static com.antonsulzhenko.algorithms.concurrency.Utils.println;
import static com.antonsulzhenko.algorithms.concurrency.Utils.randInt;

/**
 * The class Transfer.
 *
 * @author Anton Sulzhenko
 */
public class Transfer implements Callable<Boolean> {

    private static final int WAIT_TIME = 3000;

    private Account acc1;
    private Account acc2;
    private int amount;

    public Transfer(Account acc1, Account acc2, int amount) {
        this.acc1 = acc1;
        this.acc2 = acc2;
        this.amount = amount;
    }

    @Override
    public Boolean call() throws InsufficientFundsException, InterruptedException {
        if (acc1.getLock().tryLock(WAIT_TIME, TimeUnit.MILLISECONDS)) {
            println("Locked. " + acc1.toString());
            try {
                if (acc2.getLock().tryLock(WAIT_TIME, TimeUnit.MILLISECONDS)) {
                    println("Locked. " + acc2.toString());
                    try {
                        transfer(acc1, acc2, amount);

                        wait(randInt(5000, 8000));

                        return true;

                    } finally {
                        acc2.getLock().unlock();
                        println("Unlocked. " + acc2.toString());
                    }
                } else {
                    println("Unable to lock. " + acc2.toString());
                    acc2.incFailCounter();
                }
            } finally {
                acc1.getLock().unlock();
                println("Unlocked. " + acc1.toString());
            }
        } else {
            println("Unable to lock. " + acc1.toString());
            acc1.incFailCounter();
        }

        return false;
    }

    private void transfer(Account acc1, Account acc2, int amount) {
        if (acc1.getBalance() < amount) {
            throw new InsufficientFundsException();
        }

        acc1.withdraw(amount);
        acc2.deposit(amount);

        println("Transfer completed:");
        println(acc1.toString());
        println(acc2.toString());
    }
}
