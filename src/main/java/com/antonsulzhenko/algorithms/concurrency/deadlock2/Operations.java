package com.antonsulzhenko.algorithms.concurrency.deadlock2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.antonsulzhenko.algorithms.concurrency.Utils.println;
import static com.antonsulzhenko.algorithms.concurrency.Utils.randInt;

/**
 * The class Operations.
 *
 * @author Anton Sulzhenko
 */
public class Operations {

    private static final int WAIT_TIME = 3000;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) throws InterruptedException {
        final Account a = new Account(1000);
        final Account b = new Account(2000);

//        syncTransfer(a, b);
        threadPoolTransfer(a, b);
    }

    private static void threadPoolTransfer(Account a, Account b) throws InterruptedException {
        ExecutorService executor = Executors.newScheduledThreadPool(1);
        for (int i = 0; i < 10; i++) {
            executor.submit(new Transfer(a, b, randInt(100, 1500)));
        }

        executor.shutdown();
        executor.awaitTermination(3, TimeUnit.MINUTES);
    }

    private static void syncTransfer(final Account a, final Account b) throws InterruptedException {
        new Thread(new Runnable() {
            @java.lang.Override
            public void run() {
                try {
                    lockTransfer(a, b, 500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        lockTransfer(b, a, 300);
    }

    static void deadlockTransfer(Account acc1, Account acc2, int amount) throws InsufficientFundsException {
        synchronized (acc1) {
            println("Locked. " + acc1.toString());
            sleep(1000);
            synchronized (acc2) {
                println("Locked. " + acc2.toString());
                transfer(acc1, acc2, amount);
            }
            println("Unlocked. " + acc2.toString());
        }
        println("Unlocked. " + acc1.toString());
    }

    static void lockTransfer(Account acc1, Account acc2, int amount) throws InsufficientFundsException, InterruptedException {
        if (acc1.getLock().tryLock(WAIT_TIME, TimeUnit.MILLISECONDS)) {
            println("Locked. " + acc1.toString());
//            sleep(1000);
            try {
                if (acc2.getLock().tryLock(WAIT_TIME, TimeUnit.MILLISECONDS)) {
                    println("Locked. " + acc2.toString());
                    try {
                        transfer(acc1, acc2, amount);
                    } finally {
                        acc2.getLock().unlock();
                        println("Unlocked. " + acc2.toString());
                    }
                } else {
                    println("Unable to lock. " + acc2.toString());
                    acc2.incFailCounter();
                }
            } finally {
                acc1.getLock().unlock();
                println("Unlocked. " + acc1.toString());
            }
        } else {
            println("Unable to lock. " + acc1.toString());
            acc1.incFailCounter();
        }
    }

    private static void transfer(Account acc1, Account acc2, int amount) {
        if (acc1.getBalance() < amount) {
            throw new InsufficientFundsException();
        }

        acc1.withdraw(amount);
        acc2.deposit(amount);

        println("Transfer completed:");
        println(acc1.toString());
        println(acc2.toString());
    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
