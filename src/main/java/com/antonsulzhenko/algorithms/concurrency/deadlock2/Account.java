package com.antonsulzhenko.algorithms.concurrency.deadlock2;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The class Account.
 *
 * @author Anton Sulzhenko
 */
public class Account {

    private Lock lock = new ReentrantLock();

    private String id;

    private int balance;

    private AtomicInteger failCounter;

    /**
     * Instantiates a new Account.
     *
     * @param balance the balance
     */
    public Account(int balance) {
        this.balance = balance;
        this.id = UUID.randomUUID().toString();
    }

    /**
     * Withdraw void.
     *
     * @param amount the amount
     */
    public void withdraw(int amount) {
        balance -= amount;
    }

    /**
     * Deposit void.
     *
     * @param amount the amount
     */
    public void deposit(int amount) {
        balance += amount;
    }

    /**
     * Gets balance.
     *
     * @return the balance
     */
    public int getBalance() {
        return balance;
    }

    public Lock getLock() {
        return lock;
    }

    public void incFailCounter() {
        failCounter.incrementAndGet();
    }

    public int getFailCounter() {
        return failCounter.get();
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", balance=" + balance +
                '}';
    }
}
