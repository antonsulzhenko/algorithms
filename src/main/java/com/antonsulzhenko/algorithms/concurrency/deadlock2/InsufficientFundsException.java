package com.antonsulzhenko.algorithms.concurrency.deadlock2;

/**
 * The class InsufficientFundsException.
 *
 * @author Anton Sulzhenko
 */
public class InsufficientFundsException extends RuntimeException {
    /**
     * Instantiates a new Insufficient funds exception.
     */
    public InsufficientFundsException() {
    }

    /**
     * Instantiates a new Insufficient funds exception.
     *
     * @param message the message
     */
    public InsufficientFundsException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Insufficient funds exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public InsufficientFundsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Insufficient funds exception.
     *
     * @param cause the cause
     */
    public InsufficientFundsException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new Insufficient funds exception.
     *
     * @param message the message
     * @param cause the cause
     * @param enableSuppression the enable suppression
     * @param writableStackTrace the writable stack trace
     */
    public InsufficientFundsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
