package com.antonsulzhenko.algorithms.concurrency;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

/**
 * The class Utils.
 *
 * @author Anton Sulzhenko
 */
public class Utils {

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");

    private static final Random random = new Random();

    private Utils() {}

    public static void println(String str) {
        String time = DATE_FORMAT.format(new Date());
        System.out.println(time + " [" + Thread.currentThread().getName() + "] - " + str);
    }

    public static int randInt(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

}
