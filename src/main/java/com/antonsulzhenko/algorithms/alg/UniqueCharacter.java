package com.antonsulzhenko.algorithms.alg;

/**
 * The class UniqueCharacter.
 *
 * @author Anton Sulzhenko
 */
public class UniqueCharacter {

    /**
     * The constant ASCII_CHARACTERS_COUNT.
     */
    public static final int ASCII_CHARACTERS_COUNT = 256;

    public static void main(String[] args) {
        String asciiCharacters = getASCIICharacters();

        System.out.println(isUniqueChars1(asciiCharacters));
        System.out.println(isUniqueChars2("asd"));
    }

    private static boolean isUniqueChars1(String str) {
        if (str.length() > ASCII_CHARACTERS_COUNT) {
            return false;
        }
        boolean[] charSet = new boolean[ASCII_CHARACTERS_COUNT];
        for (int i = 0; i < str.length(); i++) {
            int val = str.codePointAt(i);
            if (charSet[val]) {
                return false;
            }
            charSet[val] = true;
        }
        return true;
    }

    //TODO: investigate int bit operations
    public static boolean isUniqueChars2(String str) {
        if (str.length() > 26) {
            return false;
        }
        int checker = 0;
        for (int i = 0; i < str.length(); i++) {
            int val = str.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) return false;
            checker |= (1 << val);
        }
        return true;
    }

    private static String getASCIICharacters() {
        StringBuilder builder = new StringBuilder(ASCII_CHARACTERS_COUNT);
        for (int i = 0; i < ASCII_CHARACTERS_COUNT; i++) {
            builder.append((char)i);
        }
        return builder.toString();
    }
}
