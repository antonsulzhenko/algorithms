package com.antonsulzhenko.algorithms.alg;

/**
 * The class ReverseString.
 *
 * @author Anton Sulzhenko
 */
public class ReverseString {

    public static String reverseString(String str) {
        if (str == null || str.length() == 1) {
            return str;
        }

        int lastCharIndex = str.length() - 1;

        String truncatedString = str.substring(0, lastCharIndex);

        return str.charAt(lastCharIndex) + reverseString(truncatedString);
    }

    public static void main(String[] args) {
        String str = "Anton Sulzhenko";

        String newStr = reverseString(str);

        System.out.println(newStr);
    }
}
