package com.antonsulzhenko.algorithms.alg.oneloop;

/**
 * The class FindNonTripleRepeat.
 *
 * В последовательности записаны целые числа.
 * Число X встречается один или два раза, остальные числа — по три раза. Найти число X.
 * Для простоты считаем, что числа неотрицательные.
 *
 * @author Anton Sulzhenko
 */
public class FindNonTripleRepeat {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        int[] arr = new int[]{3,3,1,1,1,2,2,2,4,4,4,5,5,5};
        System.out.println(findNonTriple(arr));
    }

    private static int findNonTriple(int[] arr) {
        int a=0,b=0;
        for(int c : arr) {
            a^=~b&c;
            b^=~a&c;
        }
        return a|b;
    }
}
