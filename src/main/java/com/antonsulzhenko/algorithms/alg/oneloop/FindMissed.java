package com.antonsulzhenko.algorithms.alg.oneloop;

/**
 * The class FindMissed.
 *
 * В последовательности записаны целые числа от 1 до N в произвольном порядке,
 * но одно из чисел пропущено (остальные встречаются ровно по одному разу).
 * N заранее неизвестно. Определить пропущенное число
 *
 * @author Anton Sulzhenko
 */
public class FindMissed {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        int[] arr = new int[]{1,3,6,4,2,7};
        System.out.println(findMissed(arr));
    }

    private static int findMissed(int[] arr) {
        int sum = 0;
        int n = 1;

        for (int i = 0; i < arr.length; i++) {
            n++;
            sum += arr[i];
        }

        return findSum(n) - sum;
    }

    private static int findSum(int count) {
        return (count*(count+1))/2;
    }
}
