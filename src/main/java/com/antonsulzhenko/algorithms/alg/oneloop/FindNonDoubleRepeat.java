package com.antonsulzhenko.algorithms.alg.oneloop;

/**
 * The class FindNonDoubleRepeat.
 *
 * В последовательности записаны целые числа.
 * Одно из чисел встречается ровно один раз, остальные — по два раза.
 * Найти число, которое встречается один раз.
 *
 * @author Anton Sulzhenko
 */
public class FindNonDoubleRepeat {
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        int[] arr = new int[]{5,1,1,2,2,3,3,4,4,7,7,6,6};
        System.out.println(findMissed(arr));
        System.out.println(findXOR(arr));
    }

    private static int findXOR(int[] arr) {
        int n = arr[0];
        for (int i = 1; i < arr.length; i++) {
            n^=arr[i];
        }
        return n;
    }

    private static int findMissed(int[] arr) {
        int sum = 0;
        int n = 1;
        for (int i = 0; i < arr.length; i++) {
            n++;
            sum += arr[i];
        }
        return findSum(n) - sum;
    }

    private static int findSum(int count) {
        return (count*(count+2))/4;
    }
}
