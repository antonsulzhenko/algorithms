package com.antonsulzhenko.algorithms.alg.sorting.statistic;

/**
 * The class ISortStatistic.
 *
 * @author Anton Sulzhenko
 */
public interface ISortStatistic {
    /**
     * Gets execution time.
     *
     * @return the execution time
     */
    long getExecutionTime();

    /**
     * Gets the number of occurrences.
     *
     * @return the number of occurrences
     */
    long getNumberOfOccurrences();

    /**
     * Gets number of swaps.
     *
     * @return the number of swaps
     */
    long getNumberOfSwaps();

    /**
     * Gets algorithm name.
     *
     * @return the algorithm name
     */
    String getAlgorithmName();
}
