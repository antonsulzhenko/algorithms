package com.antonsulzhenko.algorithms.alg.sorting.algorithm;

/**
 * The class BubbleSort.
 *
 * @author Anton Sulzhenko
 */
public class BubbleSort extends AbstractSort {

    private static final String ALGORITHM_NAME = "Bubble Sort";

    public void doSort(int[] arr) {
        for (int y = 0; y < arr.length - 1; y++) {
            int nextIterationsCount = arr.length - 1 - y;
            for (int i = 0; i < nextIterationsCount; i++) {
                numberOfOccurrences++;
                if (arr[i] > arr[i+1]) {
                    swapNext(arr, i);
                }
            }
        }
    }

    @Override
    protected String getAlgorithmName() {
        return ALGORITHM_NAME;
    }
}
