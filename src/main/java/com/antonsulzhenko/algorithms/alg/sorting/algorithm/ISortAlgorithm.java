package com.antonsulzhenko.algorithms.alg.sorting.algorithm;

import com.antonsulzhenko.algorithms.alg.sorting.statistic.ISortStatistic;

/**
 * The class ISortAlgorithm.
 *
 * @author Anton Sulzhenko
 */
public interface ISortAlgorithm {
    /**
     * Sort void.
     *
     * @param arr the arr
     */
    void sort(int arr[]);

    /**
     * Gets statistic.
     *
     * @return the statistic
     */
    ISortStatistic getStatistic();
}
