package com.antonsulzhenko.algorithms.alg.sorting.algorithm;


import com.antonsulzhenko.algorithms.alg.sorting.statistic.ISortStatistic;
import com.antonsulzhenko.algorithms.alg.sorting.statistic.SortStatistic;

import static java.lang.System.currentTimeMillis;

/**
 * The class AbstractSort.
 *
 * @author Anton Sulzhenko
 */
public abstract class AbstractSort implements ISortAlgorithm {

    /**
     * The Number of occurrences.
     */
    protected long numberOfOccurrences;

    private long executionTime;

    private long swaps;

    public void sort(int[] arr) {
        long start = currentTimeMillis();
        doSort(arr);
        executionTime = currentTimeMillis() - start;
    }

    protected void swap(int arr[], int i1, int i2) {
        int y = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = y;

        swaps++;
    }

    protected void swapPrev(int[] arr, int i) {
        int y = arr[i];
        arr[i] = arr[i-1];
        arr[i-1] = y;

        swaps++;
    }

    protected void swapNext(int[] arr, int i) {
        int y = arr[i];
        arr[i] = arr[i+1];
        arr[i+1] = y;

        swaps++;
    }

    protected abstract void doSort(int[] arr);

    protected abstract String getAlgorithmName();

    @Override
    public ISortStatistic getStatistic() {
        return new SortStatistic(getAlgorithmName(), executionTime, numberOfOccurrences, swaps);
    }
}
