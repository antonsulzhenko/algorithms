package com.antonsulzhenko.algorithms.alg.sorting;

import com.antonsulzhenko.algorithms.alg.sorting.algorithm.*;
import com.antonsulzhenko.algorithms.alg.sorting.statistic.ISortStatistic;

import java.security.InvalidParameterException;
import java.util.Random;

/**
 * The class SortChecker.
 *
 * @author Anton Sulzhenko
 */
public class SortChecker {

    private int size;
    private ArrayType arrayType;
    private int[] arr;

    public SortChecker(int size, ArrayType arrayType) {
        this.size = size;
        this.arrayType = arrayType;
        this.arr = generateArr();
    }

    private void checkAlgorithm(ISortAlgorithm sort) {
        int[] newArr = new int[size];
        System.arraycopy(arr, 0, newArr, 0, size);

        sort.sort(newArr);

        checkSorting(newArr);
        printStatistic(sort.getStatistic());
    }

    private void checkSorting(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i+1]) {
                System.out.println("!!! Result: incorrect !!!");
                return;
            }
        }
    }

    private int[] generateArr() {
        switch (arrayType) {
            case BAD: return initBadArray(size);
            case RANDOM: return initRandomArray(size);
            default: throw new InvalidParameterException("Unsupported array type.");
        }
    }

    private void printStatistic(ISortStatistic sortStatistic) {
        System.out.println("Algorithm: " + sortStatistic.getAlgorithmName());
        System.out.println("Execution time: " + sortStatistic.getExecutionTime() + " mills");
        System.out.println("Number of occurrences: " + sortStatistic.getNumberOfOccurrences());
        System.out.println("Number of swaps: " + sortStatistic.getNumberOfSwaps());
        System.out.println("============================================");
    }

    public int[] initBadArray(int size) {
        int[] arr = new int[size];
        int index = 0;
        for (int i = size; i > 0; i--) {
            arr[index++] = i;
        }
        return arr;
    }

    public int[] initRandomArray(int size) {
        Random random = new Random();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(size);
        }
        return arr;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        int size = 10;
        ArrayType arrayType = ArrayType.RANDOM;

        SortChecker sortChecker = new SortChecker(size, arrayType);
        sortChecker.checkAlgorithm(new BubbleSort());
        sortChecker.checkAlgorithm(new InsertionSort());
        sortChecker.checkAlgorithm(new SelectionSort());
        sortChecker.checkAlgorithm(new ShellSort(new InsertionSort()));
    }

    private enum ArrayType {
        BAD, RANDOM
    }
}
