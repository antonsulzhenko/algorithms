package com.antonsulzhenko.algorithms.alg.sorting.statistic;

/**
 * The class SortStatistic.
 *
 * @author Anton Sulzhenko
 */
public class SortStatistic implements ISortStatistic {

    private String algorithmName;

    private long executionTime;

    private long numberOfOccurrences;

    private long swaps;

    /**
     * Instantiates a new Sort statistic.
     * @param algorithmName the algorithm name
     * @param executionTime the execution time
     * @param numberOfOccurrences the number of occurrences
     * @param swaps the swaps
     */
    public SortStatistic(String algorithmName, long executionTime, long numberOfOccurrences, long swaps) {
        this.algorithmName = algorithmName;
        this.executionTime = executionTime;
        this.numberOfOccurrences = numberOfOccurrences;
        this.swaps = swaps;
    }

    @Override
    public long getExecutionTime() {
        return executionTime;
    }

    @Override
    public long getNumberOfOccurrences() {
        return numberOfOccurrences;
    }

    @Override
    public long getNumberOfSwaps() {
        return swaps;
    }

    @Override
    public String getAlgorithmName() {
        return algorithmName;
    }
}
