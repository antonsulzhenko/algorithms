package com.antonsulzhenko.algorithms.alg.sorting.algorithm;

/**
 * The class SelectionSort.
 *
 * @author Anton Sulzhenko
 */
public class SelectionSort extends AbstractSort {

    private static final String ALGORITHM_NAME = "Selection Sort";

    @Override
    protected void doSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int lessIndex = i;
            for (int j = i; j < arr.length - 1; j++) {
                numberOfOccurrences++;
                if (arr[lessIndex] > arr[j+1]) {
                    lessIndex = j+1;
                }
            }
            swap(arr, i, lessIndex);
        }
    }

    @Override
    protected String getAlgorithmName() {
        return ALGORITHM_NAME;
    }
}
