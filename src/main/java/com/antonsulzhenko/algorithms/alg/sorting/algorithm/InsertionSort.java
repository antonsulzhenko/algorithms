package com.antonsulzhenko.algorithms.alg.sorting.algorithm;

/**
 * The class InsertionSort.
 *
 * @author Anton Sulzhenko
 */
public class InsertionSort extends AbstractSort {

    private static final String ALGORITHM_NAME = "Insertion Sort";

    public void doSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int index = i;
            while (index > 0 && arr[index] < arr[index-1]) {
                numberOfOccurrences++;
                swapPrev(arr, index);
                index--;
            }
        }
    }

    @Override
    protected String getAlgorithmName() {
        return ALGORITHM_NAME;
    }
}
