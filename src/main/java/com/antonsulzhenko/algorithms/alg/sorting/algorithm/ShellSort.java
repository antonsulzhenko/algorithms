package com.antonsulzhenko.algorithms.alg.sorting.algorithm;

/**
 * The class ShellSort.
 *
 * @author Anton Sulzhenko
 */
public class ShellSort extends AbstractSort {

    public static final String ALGORITHM_NAME = "Shell Sort";

    private ISortAlgorithm easySort;

    public ShellSort(ISortAlgorithm easySort) {
        this.easySort = easySort;
    }

    @Override
    protected void doSort(int[] arr) {
        int gap = arr.length / 2;

        while (gap > 1) {

            gap /= 2;
        }

        easySort.sort(arr);
    }

    @Override
    protected String getAlgorithmName() {
        return ALGORITHM_NAME + " + " + easySort.getStatistic().getAlgorithmName();
    }
}
